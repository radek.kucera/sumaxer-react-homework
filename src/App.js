import React, { Component } from "react";
import "./App.css";
import { Provider } from "react-redux";
//import { Switch, Router, Route } from 'react-router-dom';
import configureStore from "./store/configureStore";

import Stats from "./Components/Stats/Stats";
import Desk from "./Components/Desk/Desk";
import AsideDeck from "./Components/AsideDeck/AsideDeck";

const initialState = {};

const store = configureStore(initialState);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <h1>SuMaxer Game</h1>
        <Stats />
        <div className="table">
          <AsideDeck />
          <Desk />
        </div>
      </Provider>
    );
  }
}

export default App;
