import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import * as Data from "./Data";
 
export default function configureStore (initialState) {
   
   const middleware = [];
   const enhancers = [];
   const rootReducer = () => combineReducers({
       Data: Data.reducer,
   });
 
   return createStore(
       rootReducer(),
       initialState,
       compose(applyMiddleware(...middleware), ...enhancers)
     );
}
