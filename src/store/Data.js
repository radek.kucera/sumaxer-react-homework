import { suMaxer } from "../model/index";

const CHANGEDATA = "CHANGEDATA";

const initialState = { Data: suMaxer.initGame() };

console.log(initialState);

export const actionCreators = {
  changeData: (Data, Card) => {
    let su = new suMaxer(Data);
    su.Turn(Card);
    if (su.turnsLeft <= 0) {
      alert(`KONEC! Vaše body: ${su.sum}`);
      return { type: CHANGEDATA, payload: suMaxer.initGame() };
    }
    return { type: CHANGEDATA, payload: su };
  }
};

export const reducer = (state, action) => {
  state = state || initialState;
  switch (action.type) {
    case CHANGEDATA: {
      return { ...state, Data: action.payload };
    }
    default:
      return state;
  }
};
