import React, { Component } from "react";
import "./AsideDeck.css";
import Card from "../Card/Card";
import { connect } from "react-redux";
import { actionCreators as acData } from "../../store/Data";

class AsideDeck extends Component {
  render() {
    const cards = this.props.Data.selected.map(card => {
      return <Card key={"Aside-" + card} turned={true} num={card}></Card>;
    });
    return (
      <section className="asideDeck">
        {cards}
        <div className="stats">
          <p>{this.props.Data.selected.length}</p>
        </div>
      </section>
    );
  }
}

export default connect(
  state => {
    return {
      Data: state.Data.Data
    };
  },
  {
    changeData: acData.changeData
  }
)(AsideDeck);
