import React, { Component } from "react";
import "./Desk.css";
import { connect } from "react-redux";
import { actionCreators as acData } from "../../store/Data";

import Card from "../Card/Card";

class Desk extends Component {
  render() {
    const cards = this.props.Data.board.map(card => {
      return (
        <Card
          key={card.value}
          num={card.value}
          click={() => {
            this.props.changeData(this.props.Data, card.value);
          }}
          turned={card.turned}
        ></Card>
      );
    });
    return <section className="desk">{cards}</section>;
  }
}

export default connect(
  state => {
    return {
      Data: state.Data.Data
    };
  },
  {
    changeData: acData.changeData
  }
)(Desk);
