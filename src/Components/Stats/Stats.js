import React, { Component } from "react";
import "./Stats.css";
import { connect } from "react-redux";
import { actionCreators as acData } from "../../store/Data";

class Stats extends Component {
  render() {
    return (
      <aside className="stats endGame">
        <p>
          Tahů do konce: <span>{this.props.Data.maxTurned}</span>
        </p>
        <p>
          Ještě lze vybrat: <span>{this.props.Data.turnsLeft}</span>
        </p>
        <p>
          Celkem bodů: <span>{this.props.Data.sum}</span>
        </p>
      </aside>
    );
  }
}

export default connect(
  state => {
    return {
      Data: state.Data.Data
    };
  },
  {
    changeData: acData.changeData
  }
)(Stats);
