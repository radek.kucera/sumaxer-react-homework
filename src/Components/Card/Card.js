import React, { Component } from "react";
import "./Card.css";

class Card extends Component {
  render() {
    if (this.props.turned === true)
      return (
        <figure onClick={this.props.click} className="item selected">
          <div className="face">
            <p>{this.props.num}</p>
          </div>
        </figure>
      );
    else {
      return (
        <figure onClick={this.props.click} className="item">
          <div className="back">
            <div className="yin-yang"></div>
            <p>SUMAxer 22</p>
          </div>
        </figure>
      );
    }
  }
}

export default Card;
